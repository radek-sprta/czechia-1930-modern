# Czechia 1930 Modern

Czechia scenario made with modern set of mods (FIRS 3, Total Town Replacement etc.).

It's based on CS 1930 Scenario.

- Climate: Temperate
- Size: 256x512
- Starting date: 1930-01-01

This is a scenario for [OpenTTD](https://www.openttd.org)
